"use strict";

// TODO: Build a UI page for displaying NOUNS AND ADJECTIVES
// Let it display all the properties of the NOUN/ADJECTIVE
// This will help me visually debug the NOUN/ADJECTIVE SYSTEM
const Tags = Object.freeze({
	ALLFORMS: "allforms",
	IRREGULAR: "irregular",
	ADJECTIVAL: "adjectival",
	DEFECTIVE: {
		SUBJECT: "defective.subject",
	},
	NOUNCLASS: {
		FIRST: "nounclass.first",
		SECOND: "nounclass.second",
		THIRD: "nounclass.third",
		FOURTH: "nounclass.fourth",
		FIFTH: "nounclass.fifth",
		PRONOUN: "nounclass.pronoun",
		INDECLINABLE: "nounclass.indeclinable",
	},
	ADJCLASS: {
		FIRSTSECOND: "adjclass.firstsecond",
		THIRD: "adjclass.third",
	},
	DECLENSION: {
		FIRST: 0,
		SECOND: {
			US: 1,
			RNOCONTRACT: 2,
			ERCONTRACT: 3,
			UM: 4,
		},
		THIRD: {
			CONSMF: 5,
			CONSN: 6,
			ISTEMMF: 7,
			ISTEMN: 8,
			BOSSUS: 9,
		},
		FOURTH: {
			USMF: 10,
			ŪN: 11,
		},
		FIFTH: {
			IĒS: 12,
			ĒS: 13,
		},
		INDECLINABLE: 14,
		ADJECTIVAL: {
			ĪUSM: 15,
			ĪUSRNOCONTRACT: 16,
			ĪUSERCONTRACT: 17,
			ĪUSF: 18,
			ĪUSN: 19,
			ISTEMMF2: 20,
			ISTEMN2: 21,
			ISTEMMF: 22,
			/* ISTEMN: SAME AS DECLENSION.THIRD.ISTEMN */
		},
		PRONOUN: {
			// Crazy Latin correlatives, I'm sure that there could be more here...
			PERSONAL: 23,
			REFLEXIVE: 24,
			RELATIVE: "declension.pronoun.relative", /* QUI, QUISQUIS, QUICUMQUE */
			INTERROGATIVE: "declension.pronoun.interrogative", /* QUIS, */
			DEMONSTRATIVE: "declension.pronoun.demonstrative", /* IS, ISTIC */
			PRONOMIALDEMONSTRATIVE: "declension.pronoun.prenomialdemonstrative", /* ISTE, IPSE */ 
			DETERMINER: "declension.pronoun.determiner", /* IDEM, HIC, ILLE */
			DISTALDEMONSTRATIVE: "declension.pronoun.distaldemonstrative", /* ILLIC */
		},
	},
	GENDER: {
		MASCULINE: 0,
		FEMININE: 1,
		NEUTER: 2,
	},
});

const NounCase = Object.freeze({
	ISSINGULAR: (n) => (n < 6 || n === 12),
	ISPLURAL: (n) => (n >= 6 && !(n === 12)),
	SINGULAR: {
		NOMINATIVE: 0,
		GENITIVE: 1,
		DATIVE: 2,
		ACCUSATIVE: 3,
		ABLATIVE: 4,
		VOCATIVE: 5,
		LOCATIVE: 12,
	},
	PLURAL: {
		NOMINATIVE: 6,
		GENITIVE: 7,
		DATIVE: 8,
		ACCUSATIVE: 9,
		ABLATIVE: 10,
		VOCATIVE: 11,
		LOCATIVE: 13,
	},
});

const NounDecline = Object.freeze({
	inflectall: function(parts, suffixes, declension) {
		return Array.from(suffixes, (s,i) => this.inflect(parts, s, declension, i));
	},
	inflect: function(parts, suffix, declension, casenum) {
		if (declension[casenum] == -1) {
			return "";
		} else {
			return parts[declension[casenum]] + suffix;
		}
	},
	pure: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
	third: {
		mascfem: [0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1],
		neuter: [0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1],
		bossus: [0, 1, 1, 1, 1, 0, 1, 3, 2, 1, 2, 1],
	},
	defective: {
		subject_sp: [-1, 0, 0, 0, 0, -1, -1, 0, 0, 0, 0, -1],
	},
	pronoun: {
		personal: [0, 1, 2, 3, 3, 0, 4, 5, 4, 4, 4, 4],
	},
});
// TODO: Weird Greek forms check here: https://en.wiktionary.org/wiki/Appendix:Latin_first_declension
const NounDeclensions = Object.freeze([
	{
		name: "1st Declension",
		tip: undefined,
		class: Tags.NOUNCLASS.FIRST,
		subtype: Tags.DECLENSION.FIRST,
		tags: undefined,
		forms: undefined,
		inflect: NounDecline.pure,
		suffixes: ["a", "ae", "ae", "am", "ā", "a",
		           "ae", "ārum", "īs", "ās", "īs", "ae"]
	},
	{
		name: "2nd Declension in -us",
		tip: undefined,
		class: Tags.NOUNCLASS.SECOND,
		subtype: Tags.DECLENSION.SECOND.US,
		tags: undefined,
		forms: undefined,
		inflect: NounDecline.pure,
		suffixes: ["us", "ī", "ō", "um", "ō", "e",
		           "ī", "ōrum", "īs", "ōs", "īs", "ī"]},
	{
		name: "2nd Declension in -r (-er, -ir) non-contracting",
		tip: "Add the same endings as 2nd Declension in -us except for Nom/Voc singular.",
		class: Tags.NOUNCLASS.SECOND,
		subtype: Tags.DECLENSION.SECOND.RNOCONTRACT,
		tags: undefined,
		forms: undefined,
		inflect: NounDecline.pure,
		suffixes: ["", "ī", "ō", "um", "ō", "",
		           "ī", "ōrum", "īs", "ōs", "īs", "ī"]
	},
	{
		name: "2nd Declension in -er contracting",
		tip: undefined,
		class: Tags.NOUNCLASS.SECOND,
		subtype: Tags.DECLENSION.SECOND.ERCONTRACT,
		tags: undefined,
		forms: undefined,
		inflect: NounDecline.pure,
		suffixes: ["er", "rī", "rō", "rum", "rō", "er",
		           "rī", "rōrum", "rīs", "rōs", "rīs", "rī"]
	},
	{
		name: "2nd Declension neuter in -um",
		tip: undefined,
		class: Tags.NOUNCLASS.SECOND,
		subtype:  Tags.DECLENSION.SECOND.UM,
		tags: undefined,
		forms: undefined,
		inflect: NounDecline.pure,
		suffixes: ["um", "ī", "ō", "um", "ō", "um",
		           "a", "ōrum", "īs", "a", "īs", "a"]
	},
	{
		name: "3rd Declension masculine/feminine consonant stem",
		tip: undefined,
		class: Tags.NOUNCLASS.THIRD,
		subtype: Tags.DECLENSION.THIRD.CONSMF,
		tags: undefined,
		forms: undefined,
		inflect: NounDecline.third.mascfem,
		suffixes: ["", "is", "ī", "em", "e", "",
		           "ēs", "um", "ibus", "ēs", "ibus", "ēs"]
	},
	{
		name: "3rd Declension neuter consonant stem",
		tip: undefined,
		class: Tags.NOUNCLASS.THIRD,
		subtype: Tags.DECLENSION.THIRD.CONSN,
		tags: undefined,
		forms: undefined,
		inflect: NounDecline.third.neuter,
		suffixes: ["", "is", "ī", "", "e", "",
		           "a", "um", "ibus", "a", "ibus", "a"]
	},
	{
		name: "3rd Declension masculine/feminine -i- stem",
		tip: "The same endings as consonant stem except Gen plural has an i.",
		class: Tags.NOUNCLASS.THIRD,
		subtype: Tags.DECLENSION.THIRD.ISTEMMF,
		tags: undefined,
		forms: undefined,
		inflect: NounDecline.third.mascfem,
		suffixes: ["", "is", "ī", "em", "e", "",
		           "ēs", "ium", "ibus", "ēs", "ibus", "ēs"]
	},
	{
		name: "3rd Declension neuter -i- stem",
		tip: "The same endings as consonant stem except Abl singular and Nom/Gen/Acc/Voc plural have an i.",
		class: Tags.NOUNCLASS.THIRD,
		subtype: Tags.DECLENSION.THIRD.ISTEMN,
		tags: undefined,
		forms: undefined,
		inflect: NounDecline.third.neuter,
		suffixes: ["", "is", "ī", "", "ī", "",
		           "ia", "ium", "ibus", "ia", "ibus", "ia"]
	},
	{
		name: "3rd Declension irregular bōs and sūs",
		tip: "Irregular Dat/Abl plural for both and Gen plural is missing the v for bōs.",
		class: Tags.NOUNCLASS.THIRD,
		subtype: Tags.DECLENSION.THIRD.BOSSUS,
		tags: [Tags.ALLFORMS, Tags.IRREGULAR],
		forms: [["bō", "bov", "bō", "bo"], ["bō", "bov", "bū", "bo"], ["sū", "su", "sui", "su"], ["sū", "su", "sū", "su"], ["sū", "su", "su", "su"]],
		inflect: NounDecline.third.bossus,
		suffixes: ["s", "is", "ī", "em", "e", "s",
		           "ēs", "um", "bus", "ēs", "bus", "ēs"]
	},
	{
		name: "4th Declension masculine/feminine",
		tip: undefined,
		class: Tags.NOUNCLASS.FOURTH,
		subtype: Tags.DECLENSION.FOURTH.USMF,
		tags: undefined,
		forms: undefined,
		inflect: NounDecline.pure,
		suffixes: ["us", "ūs", "uī", "um", "ū", "us",
		           "ūs", "uum", "ibus", "ūs", "ibus", "ūs"]
	},
	{
		name: "4th Declension neuter",
		tip: undefined,
		class: Tags.NOUNCLASS.FOURTH,
		subtype: Tags.DECLENSION.FOURTH.ŪN,
		tags: undefined,
		forms: undefined,
		inflect: NounDecline.pure, 
		suffixes: ["ū", "ūs", "ū", "ū", "ū", "ū",
		           "ua", "uum", "ibus", "ua", "ibus", "ua"]
	},
	{
		name: "5th Declension in -iēs",
		tip: undefined,
		class: Tags.NOUNCLASS.FIFTH,
		subtype: Tags.DECLENSION.FIFTH.IĒS,
		tags: undefined,
		forms: undefined,
		inflect: NounDecline.pure,
		suffixes: ["ēs", "ēī", "ēī", "em", "ē", "ēs",
		           "ēs", "ērum", "ēbus", "ēs", "ēbus", "ēs"]
	},
	{
		name: "5th Declension in -ēs",
		tip: "Like 5th declension in -iēs except no macron on Gen/Dat singular final e.",
		class: Tags.NOUNCLASS.FIFTH,
		subtype: Tags.DECLENSION.FIFTH.ĒS,
		tags: undefined,
		forms: undefined,
		inflect: NounDecline.pure,
		suffixes: ["ēs", "eī", "eī", "em", "ē", "ēs",
		           "ēs", "ērum", "ēbus", "ēs", "ēbus", "ēs"]
	},
	{
		name: "Indeclinable",
		tip: undefined,
		class: Tags.NOUNCLASS.INDECLINABLE,
		subtype: Tags.DECLENSION.INDECLINABLE,
		tags: undefined,
		forms: undefined,
		inflect: NounDecline.pure,
		suffixes: ["", "", "", "", "", "",
		           "", "", "", "", "", ""]
	},
	{
		name: "Adjectival Declension (Gen singular -īus) Masculine",
		tip: undefined,
		class: Tags.NOUNCLASS.SECOND,
		subtype: Tags.DECLENSION.ADJECTIVAL.ĪUSM,
		tags: [Tags.ADJECTIVAL],
		forms: undefined,
		inflect: NounDecline.pure,
		suffixes: ["us", "īus", "ī", "um", "ō", "e",
		           "ī", "ōrum", "īs", "ōs", "īs", "ī"]
	},
	{
		name: "Adjectival Declension (Gen singular -īus, non-contracting -r) Masculine",
		tip: undefined,
		class: Tags.NOUNCLASS.SECOND,
		subtype: Tags.DECLENSION.ADJECTIVAL.ĪUSRNOCONTRACT,
		tags: [Tags.ADJECTIVAL],
		forms: undefined,
		inflect: NounDecline.pure,
		suffixes: ["", "īus", "ī", "um", "ō", "",
		           "ī", "ōrum", "īs", "ōs", "īs", "ī"]
	},
	{
		name: "Adjectival Declension (Gen singular -īus, contracting -er) Masculine",
		tip: undefined,
		class: Tags.NOUNCLASS.SECOND,
		subtype: Tags.DECLENSION.ADJECTIVAL.ĪUSERCONTRACT,
		tags: [Tags.ADJECTIVAL],
		forms: undefined,
		inflect: NounDecline.pure,
		suffixes: ["er", "rīus", "rī", "rum", "rō", "er",
		           "rī", "rōrum", "rīs", "rōs", "rīs", "rī"]
	},
	{
		name: "Adjectival Declension (Gen singular -īus) Feminine",
		tip: undefined,
		class: Tags.NOUNCLASS.FIRST,
		subtype: Tags.DECLENSION.ADJECTIVAL.ĪUSF,
		tags: [Tags.ADJECTIVAL],
		forms: undefined,
		inflect: NounDecline.pure,
		suffixes: ["a", "īus", "ī", "am", "ā", "a",
		           "ae", "ārum", "īs", "ās", "īs", "ae"]
	},
	{
		name: "Adjectival Declension (Gen singular -īus) Neuter",
		tip: undefined,
		class: Tags.NOUNCLASS.SECOND,
		subtype: Tags.DECLENSION.ADJECTIVAL.ĪUSN,
		tags: [Tags.ADJECTIVAL],
		forms: undefined,
		inflect: NounDecline.pure,
		suffixes: ["um", "īus", "ī", "um", "ō", "um",
		           "a", "ōrum", "īs", "a", "īs", "a"]
	},
	{ 
		name: "Adjectival 3rd Declension two termination masculine/feminine -i- stem",
		tip: "Same as non-adjectival except only a single part, -is Nom singular, -ī Abl singular.",
		class: Tags.NOUNCLASS.THIRD,
		subtype: Tags.DECLENSION.ADJECTIVAL.ISTEMMF2,
		tags: [Tags.ADJECTIVAL],
		forms: undefined,
		inflect: NounDecline.pure,
		suffixes: ["is", "is", "ī", "em", "ī", "is",
		           "ēs", "ium", "ibus", "ēs", "ibus", "ēs"] // Acc plural -īs is rare... seems like incorrect imitation of -īus stems or something?
	},
	{
		name: "Adjectival 3rd Declension two termination neuter -i- stem",
		tip: "Same as non-adjectival except singular Nom/Voc -e, Abl -e,.",
		class: Tags.NOUNCLASS.THIRD,
		subtype: Tags.DECLENSION.ADJECTIVAL.ISTEMN2,
		tags: [Tags.ADJECTIVAL],
		forms: undefined,
		inflect: NounDecline.pure,
		suffixes: ["e", "is", "ī", "e", "ī", "e",
		           "ia", "ium", "ibus", "ia", "ibus", "ia"]
	},
	{ 
		name: "Adjectival 3rd Declension one termination masculine/feminine -i- stem",
		tip: "Like 3rd declension -i- stem masculine/feminine except Abl singular -ī.",
		class: Tags.NOUNCLASS.THIRD,
		subtype: Tags.DECLENSION.ADJECTIVAL.ISTEMMF,
		tags: [Tags.ADJECTIVAL],
		forms: undefined,
		inflect: NounDecline.third.mascfem,
		suffixes: ["", "is", "ī", "em", "ī", "",
		           "ēs", "ium", "ibus", "ēs", "ibus", "ēs"] // Acc plural -īs is rare... seems like incorrect imitation of -īus stems or something?
	},
	{
		name: "1st/2nd Personal Pronouns",
		tip: "Nom/Voc singular is unique, Gen singular/plural are each unique.",
		class: Tags.NOUNCLASS.PRONOUN,
		subtype: Tags.DECLENSION.PRONOUN.PERSONAL,
		tags: [Tags.ALLFORMS],
		forms: [["ego", "me", "mih", "m", "n", "no"], ["tū", "tu", "tib", "t", "v", "ve"]],
		inflect: NounDecline.pronoun.personal,
		suffixes: ["", "ī", "i", "ē", "ē", "",
		           "ōs", "strī", "ōbīs", "ōs", "ōbīs", "ōs"]
	},
	{
		name: "Reflexive Pronoun",
		tip: "Identical singular and plural forms. Defective: no Nom/Voc forms.",
		class: Tags.NOUNCLASS.PRONOUN,
		subtype: Tags.DECLENSION.PRONOUN.REFLEXIVE,
		tags: [Tags.ALLFORMS, Tags.DEFECTIVE.SUBJECT],
		forms: [["s"]],
		inflect: NounDecline.defective.subject_sp,
		suffixes: ["", "uī", "ibi", "ē", "ē", "",
		           "", "uī", "ibi", "ē", "ē", ""]
	},
]);

function AdjectiveDecline(AdjectiveType, [mascPart, femPart, neutPart]) {
	let m = AdjectiveDeclensions[AdjectiveType].subtype[Tags.GENDER.MASCULINE];
	let f = AdjectiveDeclensions[AdjectiveType].subtype[Tags.GENDER.FEMININE];
	let n = AdjectiveDeclensions[AdjectiveType].subtype[Tags.GENDER.NEUTER];
	return [NounDecline.inflectall(mascPart, NounDeclensions[m].suffixes, NounDeclensions[m].inflect),
			NounDecline.inflectall(femPart, NounDeclensions[f].suffixes, NounDeclensions[f].inflect),
			NounDecline.inflectall(neutPart, NounDeclensions[n].suffixes, NounDeclensions[n].inflect)
	];
}

// TODO: Fit Comparative and Superlative declension somewhere into the object
// REFER TO: http://dcc.dickinson.edu/grammar/latin/declension-comparatives
const AdjectiveDeclensions = Object.freeze([
	{
		name: "1st Feminine -a, 2nd Masculine -us",
		tip: "Decline exactly as normal -a, -us, -um nouns.",
		class: Tags.ADJCLASS.FIRSTSECOND,
		subtype: [Tags.DECLENSION.SECOND.US, Tags.DECLENSION.FIRST, Tags.DECLENSION.SECOND.UM], // used to index into NounDeclensions
		tags: undefined,
		forms: undefined, // eg: bonus, bona, bonum
	},
	{
		name: "1st Feminine -ra, 2nd Masculine -r (no contract)",
		tip: "Decline exactly as normal -r for Masculine/Neuter, Add 1st declension stems to feminine.",
		class: Tags.ADJCLASS.FIRSTSECOND,
		subtype: [Tags.DECLENSION.SECOND.RNOCONTRACT, Tags.DECLENSION.FIRST, Tags.DECLENSION.SECOND.UM],
		tags: undefined,
		forms: undefined, // eg: miser, misera, miserum
	},
	{
		name: "1st Feminine -ra, 2nd Masculine/Neuter -er (contract)",
		tip: "Decline Masculine as contracting -er noun, Feminine/Neuter use the contracted form.",
		class: Tags.ADJCLASS.FIRSTSECOND,
		subtype: [Tags.DECLENSION.SECOND.ERCONTRACT, Tags.DECLENSION.FIRST, Tags.DECLENSION.SECOND.UM],
		tags: undefined,
		forms: undefined, // eg: sacer, sacra, sacrum
	},
	{
		name: "1st Feminine -a, 2nd Masculine -us (Gen singular -īus)",
		tip: "Special -īus forms.",
		class: Tags.ADJCLASS.FIRSTSECOND,
		subtype: [Tags.DECLENSION.ADJECTIVAL.ĪUSM, Tags.DECLENSION.ADJECTIVAL.ĪUSF, Tags.DECLENSION.ADJECTIVAL.ĪUSN],
		tags: undefined,
		forms: undefined, // eg: sōlus, sōla, solum
	},
	{
		name: "1st Feminine -a, 2nd Masculine non-contracting -r (Gen singular -īus)",
		tip: "Special -īus forms with -r Masculine.",
		class: Tags.ADJCLASS.FIRSTSECOND,
		subtype: [Tags.DECLENSION.ADJECTIVAL.ĪUSRNOCONTRACT, Tags.DECLENSION.ADJECTIVAL.ĪUSF, Tags.DECLENSION.ADJECTIVAL.ĪUSN],
		tags: undefined,
		forms: undefined, // eg: alter, altera, alterum
	},
	{
		name: "1st Feminine -a, 2nd Masculine contracting -er (Gen singular -īus)",
		tip: "Special -īus forms with -er contract Masculine.",
		class: Tags.ADJCLASS.FIRSTSECOND,
		subtype: [Tags.DECLENSION.ADJECTIVAL.ĪUSERCONTRACT, Tags.DECLENSION.ADJECTIVAL.ĪUSF, Tags.DECLENSION.ADJECTIVAL.ĪUSN],
		tags: undefined,
		forms: undefined, // eg: neuter, neutra, neutrum or uter utra, utrum
	},
	{
		name: "3rd Declension (two termination)",
		tip: "Nominative singulars: Masculine/Feminine in -is, Neuter in -e",
		class: Tags.ADJCLASS.THIRD,
		subtype: [Tags.DECLENSION.ADJECTIVAL.ISTEMMF2, Tags.DECLENSION.ADJECTIVAL.ISTEMMF2, Tags.DECLENSION.ADJECTIVAL.ISTEMN2],
		tags: undefined,
		forms: undefined, // eg: fortis, fortis, forte or facilis, difficilis, omnis
	},
	{
		name: "3rd Declension (one termination) -i- stem",
		tip: undefined,
		class: Tags.ADJCLASS.THIRD,
		subtype: [Tags.DECLENSION.ADJECTIVAL.ISTEMMF, Tags.DECLENSION.ADJECTIVAL.ISTEMMF, Tags.DECLENSION.THIRD.ISTEMN],
		tags: undefined,
		forms: undefined, // eg: senex, senis or potens, potentis
	},
	{
		name: "3rd Declension (one termination) consonant stem",
		tip: undefined,
		class: Tags.ADJCLASS.THIRD,
		subtype: [Tags.DECLENSION.THIRD.CONSMF, Tags.DECLENSION.THIRD.CONSMF, Tags.DECLENSION.THIRD.CONSN],
		tags: undefined,
		forms: undefined, // eg: vetus, veteris or pauper, pauperis or dives, divitis
	},
	{
		name: "3rd Declension (three termination)",
		tip: "Masculine like one-termination with contracting -er, Feminine and Neuter like two termination",
		class: Tags.ADJCLASS.THIRD,
		subtype: [Tags.DECLENSION.ADJECTIVAL.ISTEMMF, Tags.DECLENSION.ADJECTIVAL.ISTEMMF2, Tags.DECLENSION.ADJECTIVAL.ISTEMN2],
		tags: undefined,
		forms: undefined, // eg: acer, acris, acre
	},
	{
		name: "Indeclinable",
		tip: "Masculine like one-termination with contracting -er, Feminine and Neuter like two termination",
		class: Tags.ADJCLASS.INDECLINABLE,
		subtype: [Tags.DECLENSION.INDECLINABLE, Tags.DECLENSION.INDECLINABLE, Tags.DECLENSION.INDECLINABLE],
		tags: undefined,
		forms: undefined, // eg: satis, necesse. nēquam
	},
]);

function remove_macrons(word) {
	let wordarr = Array.from(word, (_, i) => {
		switch(word[i]) {
			case 'ā': return 'a';
			case 'ē': return 'e';
			case 'ī': return 'i';
			case 'ō': return 'o';
			case 'ū': return 'u';
			case 'ȳ': return 'y';
			case 'Ā': return 'A';
			case 'Ē': return 'E';
			case 'Ī': return 'I';
			case 'Ō': return 'O';
			case 'Ū': return 'U';
			case 'Ȳ': return "Y";
			default: return word[i];
		}
	});
	return wordarr.join('');
}

/* Maybe merge these all in one UI Location
class NounDeclensionUI {
	constructor() {
		// Abstract information
		this.declension = NounDeclension();
		// UI Information
		this.selector =;
		this.custom =;
		this.inputBoxes =;
		this.clear =;
		this.clearmistakes =;
		this.check =;
		this.correct =;
		this.answers =;
	}
}
*/
