const colNormal = 'ghostwhite';
const colSelect = 'lightgrey';
const colLocked = 'grey';
const colWrong = 'salmon';
const colRight = 'lightgreen';

function table_reset(elem_arr, clear_me) {
	if (clear_me == false) {
		for (i = 0; i < elem_arr.length; i++) {
			elem_arr[i].readOnly = false;
			elem_arr[i].style.backgroundColor = colNormal;
		}
	} else {
		for (i = 0; i < elem_arr.length; i++) {
			elem_arr[i].readOnly = false;
			elem_arr[i].style.backgroundColor = colNormal;
			elem_arr[i].value = "";
		}
	}
}

function table_clearfailed(elem_arr) {
	for (i = 0; i < elem_arr.length; i++) {
		if (elem_arr[i].style.backgroundColor == colWrong) {
			elem_arr[i].readOnly = false;
			elem_arr[i].style.backgroundColor = colNormal;
			elem_arr[i].value = "";
		}
	}
}

function table_check(elem_arr, val_arr, show_answers) {
	for (i = 0; i < elem_arr.length; i++) {
		elem_arr[i].readOnly = true;
		if (elem_arr[i].value == val_arr[i]) {
			elem_arr[i].style.backgroundColor = colRight;
		} else {
			elem_arr[i].style.backgroundColor = colWrong;
			if (show_answers == true) {
				var correct = ' (' + val_arr[i] + ')';
				if (elem_arr[i].value.includes(correct)) {} else { elem_arr[i].value += correct; }
			}
		}
	}
}

function table_answer(elem_arr, val_arr) {
	table_reset(elem_arr);
	for (i = 0; i < elem_arr.length; i++) {
		elem_arr[i].value = val_arr[i];
	}
}

function arr_inflect(val_arr, stem, flexns) {
	for (i = 0; i < flexns.length; i++) {
		val_arr[i] = stem + flexns[i];
	}
	return val_arr;
}

function table_change(elem_arr, val_arr, stem, flexns) {
	table_reset(elem_arr, true);
	arr_inflect(val_arr, stem, flexns);
}

function table_lock(elem_arr) {
	for (i = 0; i < elem_arr.length; i++) {
		elem_arr[i].readOnly = true;
		elem_arr[i].style.backgroundColor = colLocked;
		elem_arr[i].value = "";
	}
}

function table_change_sel(elem_arr, val_arr, stem, flexns, div, post) {
	if (stem == "CUSTOM") {
		div.className = "custom";
		post.className = "hideme";
		table_lock(elem_arr);
	} else {
		div.className = "hideme";
		post.className = "posttable";
		table_reset(elem_arr, true);
		arr_inflect(val_arr, stem, flexns);
	}
}

function table_change_impure_first(elem_arr, val_arr, stem, flexns) {
	if (stem.includes(",")) {
		table_reset(elem_arr, true);
		arr_inflect(val_arr, stem.substr(0, stem.indexOf(',')), flexns);
		val_arr[0] = stem.substr(stem.indexOf(',')+1, stem.length-1);
		val_arr[5] = val_arr[0];
	} else {
		table_lock(elem_arr);
	}
}

function table_change_impure_first_sel(elem_arr, val_arr, stem, flexns, div, post) {
	if (stem == "CUSTOM") {
		div.className = "custom";
		post.className = "hideme";
		table_lock(elem_arr);
	} else {
		div.className = "hideme";
		post.className = "posttable";
		table_reset(elem_arr, true);
		arr_inflect(val_arr, stem.substr(0, stem.indexOf(',')), flexns);
		val_arr[0] = stem.substr(stem.indexOf(',')+1, stem.length-1);
		val_arr[5] = val_arr[0];
	}
}

function table_change_impure_first2(elem_arr, val_arr, stem, flexns) {
	if (stem.includes(",")) {
		table_reset(elem_arr, true);
		arr_inflect(val_arr, stem.substr(0, stem.indexOf(',')), flexns);
		val_arr[0] = stem.substr(stem.indexOf(',')+1, stem.length-1);
		val_arr[3] = val_arr[0];
		val_arr[5] = val_arr[0];
	} else {
		table_lock(elem_arr);
	}
}

function table_change_impure_first2_sel(elem_arr, val_arr, stem, flexns, div, post) {
	if (stem == "CUSTOM") {
		div.className = "custom";
		post.className = "hideme";
		table_lock(elem_arr);
	} else {
		div.className = "hideme";
		post.className = "posttable";
		table_reset(elem_arr, true);
		arr_inflect(val_arr, stem.substr(0, stem.indexOf(',')), flexns);
		val_arr[0] = stem.substr(stem.indexOf(',')+1, stem.length-1);
		val_arr[3] = val_arr[0];
		val_arr[5] = val_arr[0];
	}
}


function frmt_input(t) {
	var len = t.value.length;
	if (len > 1 && t.value[len-1] == '-') {
		var t_before_dash = t.value[len-2];
		if ("aeiouyAEIOUY".includes(t_before_dash)) {
			t.value = t.value.substr(0, len - 2);
			switch (t_before_dash) {
				case 'a':
					t_before_dash = 'ā';
					break;
				case 'e':
					t_before_dash = 'ē';
					break;
				case 'i':
					t_before_dash = 'ī';
					break;
				case 'o':
					t_before_dash = 'ō';
					break;
				case 'u':
					t_before_dash = 'ū';
					break;
				case 'y':
					t_before_dash = 'ȳ';
					break;
				case 'A':
					t_before_dash = 'Ā';
					break;
				case 'E':
					t_before_dash = 'Ē';
					break;
				case 'I':
					t_before_dash = 'Ī';
					break;
				case 'O':
					t_before_dash = 'Ō';
					break;
				case 'U':
					t_before_dash = 'Ū';
					break;
				case 'Y':
					t_before_dash = 'Ȳ';
					break;
			}
			t.value += t_before_dash;
		}
	}
}

function chck_input(e, elem_arr, val_arr) {
	if (e.keyCode == 13 && elem_arr[0].style.backgroundColor != colLocked) {
			table_check(elem_arr, val_arr);
	}
}

function clck(t) {
	if (t.readOnly == true && t.style.backgroundColor == colWrong) {
		t.readOnly = false;
		t.style.backgroundColor = colSelect;
		t.value = "";
		t.focus();
	}
}

function ifcs(t) {
	if (t.readOnly == false) {
		t.style.backgroundColor = colSelect;
	} else if (t.style.backgroundColor == colWrong) {
		t.readOnly = false;
		t.style.backgroundColor = colSelect;
		t.value = "";
		t.focus();
	}
}

function ofcs(t) {
	if (t.readOnly == false) {
		t.style.backgroundColor = colNormal;
	}
}
